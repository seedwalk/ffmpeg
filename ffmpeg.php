<?php
class Ffmpeg {
	public function __construct() {
	}


	//Check if Array Selection already happened
	public static function checkSelection($arraySelect) {
		$result =LogicVideos::getByArray($arraySelect);

		if ($result->num_rows > 0) {
			$obj = $result->fetch_object();
			return $obj->filename;
		}
		else return false;
	}


	// //Inserts SQL line 
	// public static function inject($conn, $nombre = 'notset', $arraySelect = 'notSet', $sqlFilename= 'notset'){
	// 	$sql = "INSERT INTO videos (nombre, array_seleccionado, filename, created)
	// 	VALUES ('".$conn->real_escape_string($nombre)."', '".$conn->real_escape_string($arraySelect)."', '".$conn->real_escape_string($sqlFilename)."', now())";

	// 	if ($conn->query($sql) === TRUE) {
	// 	//echo "New record created successfully";
	// 	} else {
	// 		echo "Error: " . $sql . "<br>" . $conn->error;
	// 	}
	// }


	//creates the Video
	public static function createThumb($videoName, $thumbName) {
		//Sets the shell cmd to be executed
		$cmd = '"/usr/local/bin/ffmpeg" -i '.UPLOADS_FOLDER.$videoName.' -vf  "thumbnail,scale=640:360" -frames:v 1 '.UPLOADS_FOLDER.$thumbName;
		
		//Execustes de CMD
		exec($cmd,$output, $return_var);
	}


	public static function createTS($videos){
		for ($i=0; $i<count($videos); $i++) {

			$videoTsName = explode('.', $videos[$i])[0];
			$filename = UPLOADS_FOLDER.$videoTsName.'.ts';

			if (file_exists($filename)) {
				//File Exists... we are cool
			} else {
				$cmd = '"/usr/local/bin/ffmpeg" -i '.UPLOADS_FOLDER.$videos[$i].' -c copy -bsf:v h264_mp4toannexb -f mpegts '.$filename;
				//Execustes de CMD
				exec($cmd,$output, $return_var);
				//Common::DebugThis($return_var);
				//Common::DebugThis($cmd);
			} 
		}
	}



	//creates the Video
	public static function createVideo($videos, $outputFile) {
		//Sets the shell cmd to be executed
		$cmd = '"/usr/local/bin/ffmpeg" -i "concat:';
		//Here Goes the loop
		for ($i=0; $i<count($videos); $i++) {

			$videoTsName = explode('.', $videos[$i])[0];
			$filename = UPLOADS_FOLDER.$videoTsName.'.ts';

			$value =($i == 0) ? UPLOADS_FOLDER.'principio.ts|'.$filename : '|'.UPLOADS_FOLDER.'intermedio.ts|'.$filename;
			$cmd = $cmd.$value;
		}
		$cmd = $cmd.'|'.UPLOADS_FOLDER.'principio.ts" -c copy -bsf:a aac_adtstoasc '.UPLOADS_FOLDER.$outputFile;

		//Execustes de CMD
		exec($cmd,$output, $return_var);


	}
}



